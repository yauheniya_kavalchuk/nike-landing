import './style.css';
import { getLogo, getSocial, getTitle, getCart } from './api/header';
import { ProductsApi } from './api/productsClass';
import { setupHeader } from './components/header/header';
import { setupSlider } from './components/slider/slider';
import { setupProductCard } from './components/product/product';

const app = document.getElementById('app');

setupHeader(app, {
  logo: getLogo(),
  social: getSocial(),
  title: getTitle(),
  cart: getCart(),
});

setupSlider(app);

let currentProductNumber = 1;
const productsApi = new ProductsApi();

const buttonLeft = document.querySelector('.slider__button--left');
const buttonRight = document.querySelector('.slider__button--right');
buttonLeft.addEventListener('click', getPrevious);
buttonRight.addEventListener('click', getNext);

const productContainer = document.querySelector('.slider__product');

const currentProduct = productsApi.getProduct(currentProductNumber);
setupProductCard(productContainer, { ...currentProduct });

function getPrevious() {
  const length = productsApi.getLength();
  currentProductNumber =
    currentProductNumber === 1 ? length : currentProductNumber - 1;
  reloadProduct();
}

function getNext() {
  const length = productsApi.getLength();
  currentProductNumber =
    currentProductNumber === length ? 1 : currentProductNumber + 1;
  reloadProduct();
}

function reloadProduct() {
  const prevProduct = document.querySelector('.product');
  productContainer.removeChild(prevProduct);
  const currentProduct = productsApi.getProduct(currentProductNumber);
  setupProductCard(productContainer, { ...currentProduct });
}
