import './header.css';
import { createImage } from './../createImage';

export function setupHeader(parent, data) {
  const header = createHeader(data);
  parent.append(header);
}

function createHeader(data) {
  const header = document.createElement('header');
  header.classList.add('header');

  const logo = createLogo(data.logo);
  const title = createTitle(data.title);
  const icons = createIcons(data.social);
  const cart = createCart(data.cart);

  header.append(logo, title, icons, cart);

  return header;
}

function createLogo(logoData) {
  const logo = createImage(logoData);
  logo.classList.add('header__logo');
  return logo;
}

function createTitle(title) {
  const titleElem = document.createElement('p');
  titleElem.innerText = title.text;
  titleElem.classList.add('header__text');
  return titleElem;
}

function createIcons(social) {
  const container = document.createElement('div');

  social.forEach((icon) => {
    const iconElem = createImage(icon);
    iconElem.classList.add('header__icon');
    container.append(iconElem);
  });

  container.classList.add('header__social-container');

  return container;
}

function createCart(cart) {
  const cartElem = document.createElement('p');
  cartElem.innerText = `CART(${cart.itemsNumber})`;
  cartElem.classList.add('header__cart');
  return cartElem;
}
