import './product.css';
import { createImage } from './../createImage';

export function setupProductCard(parent, productData) {
  const product = createProductCard(productData);
  parent.append(product);
}

function createProductCard(productData) {
  const product = document.createElement('div');
  product.classList.add('product');

  const image = createProductImage(productData);
  const shadow = createShadow();
  const about = createProductAbout(productData);

  product.append(image, shadow, about);
  return product;
}

function createShadow() {
    const shadow = createImage({
        path: '/images/shadow.png',
        alt: 'shadow',
    });
    shadow.classList.add('product__shadow');
    return shadow;
}

function createProductImage(productData) {
  const imageContainer = document.createElement('div');
  imageContainer.classList.add('product__image-container');

  const image = createImage(productData);
  image.classList.add('product__image');

  const title = document.createElement('h2');
  title.innerText = productData.title;
  const classModifier =
    productData.fontColor === 'white'
      ? 'product__title--white'
      : 'product__title--black';
  title.classList.add('product__title', classModifier);

  const titleStroke = document.createElement('h2');
  titleStroke.innerText = productData.title;
  titleStroke.classList.add('product__title', classModifier, 'product__title--stroke');

  imageContainer.append(image, title, titleStroke);
  return imageContainer;
}

function createProductAbout(productData) {
  const aboutContainer = document.createElement('div');
  aboutContainer.classList.add('product__about');

  const price = createPrice(productData);
  const rating = createRating(productData);
  const lowRow = createLowRow(productData);

  aboutContainer.append(price, rating, lowRow);
  return aboutContainer;
}

function createPrice(productData) {
    const price = document.createElement('p');
    price.classList.add('product__price');
    price.innerText = productData.price;
    return price;
};

function createRating(productData) {
    const rating = document.createElement('div');
    rating.classList.add('product__rating');

    for(let i = 0; i < productData.rating; i++) {
        const star = createImage({
            path: '/icons/star-filled.svg',
            alt: 'filled star',
        });
        rating.append(star);
    }

    for(let i = productData.rating; i < 5; i++) {
        const star = createImage({
            path: '/icons/star.svg',
            alt: 'star',
        });
        rating.append(star);
    }

    return rating;
};

function createLowRow(productData) {
    const lowRow = document.createElement('div');
    lowRow.classList.add('product__description-container');

    const description = createDescription(productData);
    const button = createButton();

    lowRow.append(description, button);
    return lowRow;
};

function createDescription(productData){
    const description = document.createElement('p');
    description.classList.add('product__description');
    description.innerText = productData.description;
    return description;
};

function createButton(){
    const button = document.createElement('a');
    button.setAttribute('href', '#');
    button.innerText = 'BUY NOW';
    button.classList.add('product__button');
    return button;
};
