export function createImage(imgData) {
  const img = document.createElement('img');
  img.setAttribute('src', imgData.path);
  img.setAttribute('alt', imgData.alt);
  return img;
}
