import './slider.css';

export function setupSlider(parent) {
  const slider = createSlider();
  parent.append(slider);
}

function createSlider() {
  const slider = document.createElement('main');
  slider.classList.add('slider');
  const leftButton = createLeftButton();
  const rightButton = createRightButton();
  const productCard = createProductCard();
  slider.append(leftButton, productCard, rightButton);
  return slider;
}

function createButton() {
  const button = document.createElement('div');
  button.classList.add('slider__button');
  return button;
}

function createLeftButton() {
  const leftButton = createButton();
  leftButton.classList.add('slider__button--left');
  return leftButton;
}

function createRightButton() {
  const rightButton = createButton();
  rightButton.classList.add('slider__button--right');
  return rightButton;
}

function createProductCard() {
    const productCard = document.createElement('div');
    productCard.classList.add('slider__product');
    return productCard;
}