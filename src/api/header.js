const logo = {
    path: '/icons/logo.png',
    alt: 'Nike logo',
    href: '#',
};

const title = {
    text: 'WOOCOMMERCE  PRODUCT  SLIDER',
}

const cart = {
    itemsNumber: 0,
}

const social = [
    {
        path: '/icons/twitter.svg',
        alt: 'twitter logo',
        href: '#',
    },
    {
        path: '/icons/inst.svg',
        alt: 'instagram logo',
        href: '#',
    },
    {
        path: '/icons/facebook.svg',
        alt: 'facebook logo',
        href: '#',
    },
]

export function getLogo() {
    return {...logo};
}

export function getTitle() {
    return {...title};
}

export function getCart() {
    return {...cart};
}

export function getSocial() {
    return [...social];
}