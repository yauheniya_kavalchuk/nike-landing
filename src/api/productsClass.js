import productsData from './products.json';

export class ProductsApi {
  #products;

  constructor() {
    this.#products = [...productsData];
  }

  get products() {
    return this.#products;
  }

  addProduct(product) {
    this.#products.push(product);
  }

  getProduct(productID) {
    return this.#products[productID - 1];
  }

  getLength() {
    return this.#products.length;
  }
}
